<?php

namespace Itdashu\Easy\Forms;

class Element
{
    public $type;
    public $name;
    public $options;
    public $userOptions;
    public $attributes;
    private $_filters;
    private $_validations;
    private $_value;
    private $_defalutValue;

    public function __construct(string $name,$value=null,$default=null,array $options=array(),array $userOptions=array(),array $attributes=array(),array $filters=array(),array $validations=array())
    {
        $this->name = $name;
        $this->setValue($value);
        $this->setDefaultValue($default);
        $this->options = $options;
        $this->userOptions = $userOptions;
        $this->attributes = $attributes;
        foreach ($filters as $filter){
            $this->addFilter($filter);
        }
        foreach ($validations as $validation){
            $this->addValidations($validation);
        }
    }

    public function addFilter($filter)
    {

    }

    public function setValue($value): Element
    {
        $this->_value = $value;
        return $this;
    }

    public function getValue()
    {
        return $this->_value;
    }

    public function setDefaultValue($value)
    {
        $this->_defalutValue = $value;
        return $this;
    }

    public function getDefaultValue()
    {
        return $this->_defalutValue;
    }

    public function addValidations($validation)
    {

    }

    public function render()
    {

    }

    public function toArray(): array
    {

    }

    public function toJson(): string
    {

    }
}