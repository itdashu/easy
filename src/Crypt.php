<?php
/**
 * This file is part of HongWu soft.
 *
 * @link     https://www.hongwu.info
 * @document https://doc.hongwu.info
 * @contact  leno@itdashu.com
 * @license  https://www.hongwu.info/license.html
 */
namespace Itdashu\Easy;

/**
 * \Itdashu\Easy\Crypt
 *
 * Provides encryption facilities to phalcon applications
 *
 *<code>
 *  $crypt = new Phalcon\Crypt();
 *
 *  $key = 'le password';
 *  $text = 'This is a secret text';
 *
 *  $encrypted = $crypt->encrypt($text, $key);
 *
 *  echo $crypt->decrypt($encrypted, $key);
 *</code>
 *
 * @see https://github.com/scento/cphalcon/blob/1.2.6/ext/crypt.c
 */
class Crypt
{
    /**
     * Padding: Default
     *
     * @var int
     */
    const PADDING_DEFAULT = 0;

    /**
     * Padding: ANSI X923
     *
     * @var int
     */
    const PADDING_ANSI_X_923 = 1;

    /**
     * Padding: PKCS7
     *
     * @var int
     */
    const PADDING_PKCS7 = 2;

    /**
     * Padding: ISO 10126
     *
     * @var int
     */
    const PADDING_ISO_10126 = 3;

    /**
     * Padding: ISO IEC 7816-4
     *
     * @var int
     */
    const PADDING_ISO_IEC_7816_4 = 4;

    /**
     * Padding: Zero
     *
     * @var int
     */
    const PADDING_ZERO = 5;

    /**
     * Padding: Space
     *
     * @var int
     */
    const PADDING_SPACE = 6;

    /**
     * Key
     *
     * @var null
     * @access protected
     */
    protected $_key = null;

    /**
     * Mode
     *
     * @var string
     * @access protected
     */
    protected $_mode = 'cbc';

    /**
     * Cipher
     *
     * @var string
     * @access protected
     */
    protected $_cipher = 'aes256';

    /**
     * Padding
     *
     * @var int
     * @access protected
     */
    protected $_padding = 0;

    /**
     * Sets the cipher algorithm
     *
     * @param string $cipher
     * @return Crypt
     * @throws \Exception
     */
    public function setCipher(string $cipher): Crypt
    {
        if (is_string($cipher) === false) {
            throw new \Exception('Invalid parameter type.');
        }

        $this->_cipher = $cipher;

        return $this;
    }

    /**
     * Returns the current cipher
     *
     * @return string
     */
    public function getCipher(): string
    {
        return $this->_cipher;
    }

    /**
     * Sets the encrypt/decrypt mode
     *
     * @param string $mode
     * @return Crypt
     * @throws \Exception
     */
    public function setMode(string $mode): Crypt
    {
        if (is_string($mode) === false) {
            throw new \Exception('Invalid parameter type.');
        }

        $this->_mode = $mode;

        return $this;
    }

    /**
     * Returns the current encryption mode
     *
     * @return string
     */
    public function getMode(): string
    {
        return $this->_mode;
    }

    /**
     * Sets the encryption key
     *
     * @param string $key
     * @return Crypt
     * @throws \Exception
     */
    public function setKey(string $key): Crypt
    {
        if (is_scalar($key) === false) {
            throw new \Exception('Invalid parameter type.');
        }

        $this->_key = $key;

        return $this;
    }

    /**
     * Returns the encryption key
     *
     * @return string|null
     */
    public function getKey()
    {
        return $this->_key;
    }

    /**
     * @param int scheme Padding scheme
     * @return Crypt
     * @throws \Exception
     */
    public function setPadding(int $scheme): Crypt
    {
        if (is_int($scheme) === false) {
            throw new \Exception('Invalid parameter type.');
        }

        $this->_padding = $scheme;

        return $this;
    }

    /**
     * Returns the padding scheme
     * @return int
     */
    public function getPadding(): int
    {
        return $this->_padding;
    }

    /**
     * Unpad Message
     *
     * @brief Removes padding @a padding_type from @a text
     * @param string $text Message to be unpadded
     * @param string $mode Encryption mode; unpadding is applied only in CBC or ECB mode
     * @param int $blockSize Cipher block size
     * @param int $paddingType Padding scheme
     * @note If the function detects that the text was not padded, it will return it unmodified
     * @return string
     * @throws \Exception
     */
    private static function _cryptUnpadText(string $text, string $mode, int $blockSize, int $paddingType): string
    {
        if (is_string($text) === false || is_string($mode) === false ||
            is_int($blockSize) === false || is_int($paddingType) === false) {
            throw new \Exception('Invalid parameter type.');
        }

        $textLen = strlen($text);

        if (($textLen % $blockSize === 0) && ($mode === 'ecb' || $mode === 'cbc')) {
            switch ($paddingType) {
                case self::PADDING_ANSI_X_923:
                    if (ord($text[$textLen - 1]) <= $blockSize) {
                        $paddingSize = ord($text[$textLen - 1]);
                        $padding = str_repeat(chr(0), ($paddingSize - 1));
                        $padding[$paddingSize - 1] = $paddingSize;

                        if (strncmp($padding, $text . ($textLen - $paddingSize), $paddingSize)) {
                            $paddingSize = 0;
                        }
                    }
                    break;
                case self::PADDING_PKCS7:
                    if ($text[$textLen - 1] <= $blockSize) {
                        $paddingSize = ord($text[$textLen - 1]);
                        $padding = str_repeat($paddingSize, $paddingSize);
                        if (strncmp($padding, $text . ($textLen - $paddingSize), $paddingSize)) {
                            $paddingSize = 0;
                        }
                    }
                    break;
                case self::PADDING_ISO_10126:
                    $paddingSize = ord($text[$textLen - 1]);
                    break;
                case self::PADDING_ISO_IEC_7816_4:
                    $paddingSize = 0;
                    $i = $textLen - 1;
                    while ($i > 0 && $text[$i] === chr(0x00) && $paddingSize < $blockSize) {
                        ++$paddingSize;
                        --$i;
                    }

                    $paddingSize = ($text[$i] === chr(0x80)) ? ($paddingSize + 1) : 0;
                    break;
                case self::PADDING_ZERO:
                    $paddingSize = 0;
                    $i = $textLen - 1;
                    while ($i >= 0 && $text[$i] === chr(0x00) && $paddingSize <= $blockSize) {
                        ++$paddingSize;
                        --$i;
                    }
                    break;
                case self::PADDING_SPACE:
                    $paddingSize = 0;
                    $i = $textLen - 1;
                    while ($i >= 0 && $text[$i] === chr(0x20) && $paddingSize <= $blockSize) {
                        ++$paddingSize;
                        --$i;
                    }
                    break;
                default:
                    break;
            }

            if (isset($paddingSize) && $paddingSize <= $blockSize) {
                if ($paddingSize <= $textLen) {
                    return substr($text, 0, ($textLen - $paddingSize));
                } else {
                    return '';
                }
            }
        }

        return $text;
    }

    /**
     * Encrypts a text
     *
     *<code>
     *  $encrypted = $crypt->encrypt("Ultra-secret text", "encrypt password");
     *</code>
     *
     * @param string $text
     * @param string|null $key
     * @return string
     * @throws \Exception
     */
    public function encrypt(string $text, string $key = null): string
    {
        if (!function_exists('openssl_cipher_iv_length')) {
            throw new \Exception('openssl extension is required');
        }

        if (is_null($key)) {
            $encryptKey = $this->_key;
        } else {
            $encryptKey = (string)$key;
        }

        if (empty($encryptKey) === true) {
            throw new \Exception('Encryption key cannot be empty');
        }

        $ivSize = (int)openssl_cipher_iv_length($this->_cipher);

        if (strlen($encryptKey) > $ivSize) {
            throw new \Exception('Size of key too large for this algorithm');
        }

        //C++ source is always using \MCRYPT_RAND which is sometimes considered
        //as insecure. This might be because of windows compatibility with
        //PHP < 5.3.0
        $cipher = $this->_cipher;
        $mode = strtolower(substr($cipher, strrpos($cipher, '-') - strlen($cipher)));

        if (!in_array($cipher, openssl_get_cipher_methods(true))) {
            throw new \Exception('Cipher algorithm is unknown');
        }

        $ivSize = openssl_cipher_iv_length($cipher);
        if ($ivSize > 0) {
            $blockSize = $ivSize;
        } else {
            $blockSize = openssl_cipher_iv_length(str_ireplace('-' . $mode, '', $cipher));
        }
        $iv = openssl_random_pseudo_bytes($ivSize);
        $paddingType = $this->_padding;

        if ($paddingType != 0 && ($mode == 'cbc' || $mode == 'ecb')) {
            $padded = $this->_cryptPadText($text, $mode, $blockSize, $paddingType);
        } else {
            $padded = $text;
        }

        return $iv . openssl_encrypt($padded, $cipher, $encryptKey, OPENSSL_RAW_DATA, $iv);
    }

    /**
     * Pads texts before encryption
     * @param string $text
     * @param string $mode
     * @param int $blockSize
     * @param int $paddingType
     * @return string
     * @throws \Exception
     * @see http://www.di-mgt.com.au/cryptopad.html
     */
    protected function _cryptPadText(string $text, string $mode, int $blockSize, int $paddingType)
    {
        $paddingSize = 0;
        $padding = null;

        if ($mode == 'cbc' || $mode == 'ecb') {
            $paddingSize = $blockSize - (strlen($text) % $blockSize);
            if ($paddingSize >= 256) {
                throw new \Exception('Block size is bigger than 256');
            }

            switch ($paddingType) {

                case self::PADDING_ANSI_X_923:
                    $padding = str_repeat(chr(0), $paddingSize - 1) . chr($paddingSize);
                    break;

                case self::PADDING_PKCS7:
                    $padding = str_repeat(chr($paddingSize), $paddingSize);
                    break;

                case self::PADDING_ISO_10126:
                    $padding = '';
                    for ($i = 0; $i <= $paddingSize - 2; $i++) {
                        $padding .= chr(rand());
                    }
                    $padding .= chr($paddingSize);
                    break;

                case self::PADDING_ISO_IEC_7816_4:
                    $padding = chr(0x80) . str_repeat(chr(0), $paddingSize - 1);
                    break;

                case self::PADDING_ZERO:
                    $padding = str_repeat(chr(0), $paddingSize);
                    break;

                case self::PADDING_SPACE:
                    $padding = str_repeat(' ', $paddingSize);
                    break;

                default:
                    $paddingSize = 0;
                    break;
            }
        }

        if (!$paddingSize) {
            return $text;
        }

        if ($paddingSize > $blockSize) {
            throw new \Exception('Invalid padding size');
        }

        return $text . substr($padding, 0, $paddingSize);
    }

    /**
     * Decrypts an encrypted text
     *
     *<code>
     * echo $crypt->decrypt($encrypted, "decrypt password");
     *</code>
     *
     * @param string $text
     * @param string|null $key
     * @return string
     * @throws \Exception
     */
    public function decrypt(string $text, string $key = null): string
    {
        if (!function_exists('openssl_cipher_iv_length')) {
            throw new \Exception('openssl extension is required');
        }

        if (is_null($key)) {
            $decryptKey = $this->_key;
        } else {
            $decryptKey = $key;
        }

        if (empty($decryptKey)) {
            throw new \Exception('Decryption key cannot be empty');
        }

        $cipher = $this->_cipher;
        $mode = strtolower(substr($cipher, strrpos($cipher, '-') - strlen($cipher)));

        if (!in_array($cipher, openssl_get_cipher_methods(true))) {
            throw new \Exception('Cipher algorithm is unknown');
        }

        $ivSize = openssl_cipher_iv_length($cipher);
        if ($ivSize > 0) {
            $blockSize = $ivSize;
        } else {
            $blockSize = openssl_cipher_iv_length(str_ireplace('-' . $mode, '', $cipher));
        }

        $decrypted = openssl_decrypt(substr($text, $ivSize), $cipher, $decryptKey, OPENSSL_RAW_DATA, substr($text, 0, $ivSize));

        $paddingType = $this->_padding;

        if ($mode == 'cbc' || $mode == 'ecb') {
            return $this->_cryptUnpadText($decrypted, $mode, $blockSize, $paddingType);
        }

        return $decrypted;
    }

    /**
     * Encrypts a text returning the result as a base64 string
     *
     * @param string $text
     * @param string|null $key
     * @return string
     * @throws \Exception
     */
    public function encryptBase64(string $text, string $key = null): string
    {
        if (is_string($text) === false ||
            (is_string($key) === false && is_null($key) === false)) {
            throw new \Exception('Invalid parameter type.');
        }

        return base64_encode($this->encrypt($text, $key));
    }

    /**
     * Decrypt a text that is coded as a base64 string
     *
     * @param string $text
     * @param string|null $key
     * @return string
     * @throws \Exception
     */
    public function decryptBase64(string $text, string $key = null): string
    {
        if (is_string($text) === false ||
            (is_string($key) === false && is_null($key) === false)) {
            throw new \Exception('Invalid parameter type.');
        }

        return $this->decrypt(base64_decode($text), $key);
    }

    /**
     * Returns a list of available cyphers
     *
     * @return array
     */
    public function getAvailableCiphers(): array
    {
        return openssl_get_cipher_methods();
    }
}
