<?php

namespace Itdashu\Easy;

class Arr
{
  /**
   * 从多维数组中根据键名取回值
   *
   * @param array $array 被搜索的数组
   * @param string $name 查找的键名
   * @return mixed
   */
  public static function search(array $array, string $name)
  {
    if (isset($array[$name])) {
      return $array[$name];
    }
    foreach ($array as $v) {
      if (is_array($v)) {
        $array = self::search($v, $name);
        if ($array !== false) {
          return $array;
        }
      }
    }
    return false;
  }

  /**
   * 从多维数组中删除某一项目
   * @param array $data
   * @param string $name
   * @return array
   */
  public static function delete(array $data, string $name)
  {
    $output = $data;
    if (isset($data[$name])) {
      if (is_array($data[$name])) {
        $output = array_merge($data[$name], $output);
      }
      unset($output[$name]);

      return $output;
    }
    foreach ($data as $key => $value) {
      if (is_array($value)) {
        $data[$key] = self::delete($value, $name);
        if (empty($data[$key])) {
          $data[$key] = $key;
        }
      }
    }
    return $data;
  }

  /**
   * 筛选数组的值，值只能是字符串，且包含搜索字符串才会被保留
   * @param array $data 被筛选的数组
   * @param string $needle 搜索的字符串
   * @return array
   */
  public static function filter(array $data, string $needle)
  {
    $needle = explode('|', $needle);
    foreach ($data as $key => $value) {
      $value = Str::autoConvertEncoding($value);
      foreach ($needle as $item) {
        if (mb_stristr($value, $item) === false) {
          unset($data[$key]);
        }
      }
    }
    return $data;
  }

  /**
   * 筛选数组的值，值只能是字符串，且不能包含被搜索的字符串才会被保留
   * @param array $data 被筛选的数组
   * @param string $needle 搜索的字符串
   * @return array
   */
  public static function exclude(array $data, string $needle)
  {
    $needle = explode('|', $needle);
    foreach ($data as $key => $value) {
      $value = Str::autoConvertEncoding($value);
      foreach ($needle as $item) {
        if (mb_stristr($value, $item) !== false) {
          unset($data[$key]);
        }
      }
    }
    return $data;
  }

  /**
   * 解压json字符串
   * @param string $body
   * @return array
   */
  public static function jsonDecode($body): array
  {
    if (is_array($body)) {
      return $body;
    }
    return json_decode($body, true) ?: [];
  }

  /**
   * 根据二维数组中某个键名的值组成一个新的数组
   * @param array $arr
   * @param $name
   * @return array
   */
  public static function value(array $arr, $name):array
  {
    $output = [];
    foreach ($arr as $item) {
      if (isset($item[$name])) {
        $output[] = $item[$name];
      }
    }

    return $output;
  }
}