<?php
namespace Itdashu\Easy;

class Time{
    /**
     * 转换时间到 几分钟前 格式
     * @param int $timeStamp 需要转换的时间
     * @return false|string
     */
    function timeTran(int $timeStamp)
    {
        $nowTime = time();
        $showTime = $timeStamp;
        $timeStamp = date("Y-m-d", $showTime);
        $dur = $nowTime - $showTime;
        $intDur = abs($dur);
        if ($intDur < 60) {
            $output = $intDur . '秒';
        } else {
            if ($intDur < 3600) {
                $output = floor($intDur / 60) . '分钟';
            } else {
                if ($intDur < 86400) {
                    $output = floor($intDur / 3600) . '小时';
                } else {
                    if ($intDur < 259200) {
                        //3天内
                        $output = floor($intDur / 86400) . '天';
                    } else {
                        $output = $timeStamp;
                    }
                }
            }
        }
        if ($output != $timeStamp) {
            if ($dur < 0) {
                $output .= '后';
            } else {
                $output .= '前';
            }
        }
        return $output;
    }

    /**
     * 格式化时间日期或时间戳为精确到日的时间戳
     *
     * @param int|string $time
     * @return false|int
     */
    function formatTimeStampDay($time)
    {
        if (!is_int($time)) {
            $time = strtotime($time);
        }

        $timeDate = date('Y/m/d', $time);
        return strtotime($timeDate);
    }
}