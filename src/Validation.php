<?php
/**
 * This file is part of HongWu soft.
 *
 * @link     https://www.hongwu.info
 * @document https://doc.hongwu.info
 * @contact  leno@itdashu.com
 * @license  https://www.hongwu.info/license.html
 */

namespace Itdashu\Easy;

use Itdashu\Easy\Validation\Message\Group;
use Itdashu\Easy\Validation\ValidatorInterface;
use Exception;

/**
 * \Itdashu\Easy\Validation
 *
 * Allows to validate data using validators
 */
class Validation
{
    /**
     * Data
     *
     * @var null|array|object
     * @access protected
     */
    protected $_data = null;

    /**
     * Validators
     *
     * @var array
     * @access protected
     */
    protected $_validators = [];

    /**
     * Filters
     *
     * @var null|array
     * @access protected
     */
    protected $_filters = null;

    /**
     * Messages
     *
     * @var null|Group
     * @access protected
     */
    protected $_messages = null;

    /**
     * Values
     *
     * @var null
     * @access protected
     */
    protected $_values = null;

    /**
     * Itdashu\Easy\Validation constructor
     *
     * @param array|null $validators
     * @throws Exception
     */
    public function __construct($validators = [])
    {
        if (is_null($validators) === false && is_array($validators) === false) {
            throw new Exception('Validators must be an array');
        }

        $this->_validators = $validators;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function setData(array $data)
    {
        $this->_data = $data;
        return $this;
    }

    public function addByConfig($name, $config)
    {
        if (!is_null($name)) {
            return $this;
        }
        $method = '\Itdashu\Easy\Validation\Validator\\' . ucfirst($config['type']);
        $this->add($name, new $method($config));

        return $this;
    }

    /**
     * @param array $config
     * config[]['type'] 验证器名称
     * config[]['options'] 验证器选项
     * @return $this
     * @throws Exception
     */
    public function config(array $config)
    {
        foreach ($config as $field => $item) {
            foreach ($item as $rule) {
                if (is_string($rule)) {
                    $rule = [
                        'type' => $rule
                    ];
                    if (!isset($rule['options'])) {
                        $rule['options'] = [];
                    }
                    $method = '\Itdashu\Easy\Validation\Validator\\' . ucfirst($rule['type']);
                    $this->add($field, new $method($rule['options']));
                }
            }
        }
        return $this;
    }

    /**
     * Adds a validator to a field
     *
     * @param string $attribute
     * @param ValidatorInterface $validator
     * @return Validation
     * @throws Exception
     */
    public function add(string $attribute, ValidatorInterface $validator)
    {
        if (is_string($attribute) === false) {
            throw new Exception('The attribute must be a string');
        }

        if (is_object($validator) === false) {
            throw new Exception('The validator must be an object');
        }

        if (is_array($this->_validators) === false) {
            $this->_validators = [];
        }

        $this->_validators[] = [$attribute, $validator];

        return $this;
    }

    /**
     * Validate a set of data according to a set of rules
     *
     * @param array|object|null $data
     * @return Group|boolean|null
     */
    /**
     * @param null $data
     * @return bool|Group|null
     */
    public function validate($data = null)
    {
        //Clear pre-calculated values
        $this->_values = null;

        //Implicity creates a Phalcon\Validation\Message\Group object
        $messages = new Group();

        $this->_messages = $messages;
        if (is_array($data) === true || is_object($data) === true) {
            $this->_data = $data;
        }

        foreach ($this->_validators as $scope) {
            /** @var ValidatorInterface $scopeCallback */
            $scopeCallback = $scope[1];
            if ($scopeCallback->validate($this, $scope[0]) === false) {
                if ($scopeCallback->getOption('cancelOnFail') === true) {
                    break;
                }
            }
        }


        return $this->_messages;
    }

    /**
     * Returns all the filters or a specific one
     *
     * @param string|null $attribute
     * @return null|array|string
     */
    public function getFilters($attribute = null)
    {
        if (is_string($attribute) === true) {
            if (isset($this->_filters[$attribute]) === true) {
                return $this->_filters[$attribute];
            }

            return null;
        }

        return $this->_filters;
    }

    /**
     * Adds filters to the field
     * @param string $attribute
     * @param array|string $filters
     * @return Validation
     * @throws Exception
     */
    public function setFilters(string $attribute, $filters): Validation
    {
        if (is_string($attribute) === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_array($attribute) === false && is_string($attribute) === false) {
            throw new Exception('Invalid parameter type.');
        }

        $this->_filters[$attribute] = $filters;
        return $this;
    }

    /**
     * Returns the validators added to the validation
     *
     * @return array|null
     */
    public function getValidators()
    {
        return $this->_validators;
    }

    /**
     * @return Group|null
     */
    public function getMessages()
    {
        return $this->_messages;
    }

    /**
     * Appends a message to the messages list
     *
     * @param \Itdashu\Easy\Validation\Message $message
     * @return Validation
     */
    public function appendMessage($message): Validation
    {
        //@note the type check is not implemented in the original source code
        if (is_null($this->_messages) === false) {
            $this->_messages->appendMessage($message);
        }

        return $this;
    }

    /**
     * Gets the a value to validate in the array/object data source
     *
     * @param string|null $attribute
     * @return mixed
     */
    public function getValue($attribute = null)
    {
        if (is_null($attribute)) {
            return $this->_values;
        }
        $value = null;

        //Check if there is a calculated value
        if (isset($this->_values[$attribute]) === true) {
            return $this->_values[$attribute];
        }

        if (is_array($this->_data) === true) {
            if (isset($this->_data[$attribute]) === true) {
                $value = $this->_data[$attribute];
            }
        } elseif (is_object($this->_data) === true) {
            if (property_exists($this->_data, $attribute) === true) {
                $value = $this->_data->$attribute;
            }
        }

        if (is_null($value) === false) {
            if (is_array($this->_filters) === true && isset($this->_filters[$attribute])) {
                if (isset($this->_filters[$attribute]) === true) {
                    $filterService = new Filter();

                    return $filterService->sanitize($value, $this->_filters[$attribute]);
                }
            }

            //Cache the calculated value
            $this->_values[$attribute] = $value;
            return $value;
        }

        return null;
    }
}
