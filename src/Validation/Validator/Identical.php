<?php
/**
 * Identical Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.2.6
 * @package Phalcon
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Identical
 *
 * Checks if a value is identical to other
 *
 *<code>
 *use \Itdashu\Easy\Validation\Validator\Identical;
 *
 *$validator->add('terms', new Identical(array(
 *   'value'   => 'yes',
 *   'message' => 'Terms and conditions must be accepted'
 *)));
 *</code>
 *
 */
class Identical extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validator
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validator, string $field): bool
    {
        if (is_object($validator) === false ||
            $validator instanceof Validation === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_string($field) === false) {
            throw new Exception('Invalid parameter type.');
        }

        if ($validator->getValue($field) !== $this->getOption('value')) {
            $message = $this->getOption('message');
            if (empty($message) === true) {
                $message = $field . ' does not have the expected value';
            }

            $validator->appendMessage(new Message($message, $field, 'Identical'));

            return false;
        }

        return true;
    }
}
