<?php
/**
 * Callback Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.0
 * @package Itdashu
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Callback
 *
 * Checks if a value has a correct e-mail format
 *
 * <code>
 * use \Itdashu\Easy\Validation;
 * use \Itdashu\Easy\Validation\Validator\Callback as CallbackValidator;
 * use \Itdashu\Easy\Validation\Validator\Numericality as NumericalityValidator;
 *
 * $validator = new Validation();
 *
 * $validator->add(
 *     ["user", "admin"],
 *     new CallbackValidator(
 *         [
 *             "message" => "There must be only an user or admin set",
 *             "callback" => function($data) {
 *                 if (!empty($data->getUser()) && !empty($data->getAdmin())) {
 *                     return false;
 *                 }
 *
 *                 return true;
 *             }
 *         ]
 *     )
 * );
 *
 * $validator->add(
 *     "amount",
 *     new CallbackValidator(
 *         [
 *             "callback" => function($data) {
 *                 if (!empty($data->getProduct())) {
 *                     return new NumericalityValidator(
 *                         [
 *                             "message" => "Amount must be a number."
 *                         ]
 *                     );
 *                 }
 *             }
 *         ]
 *     )
 * );
 * </code>
 */
class Callback extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        $value = $validation->getValue($field);

        $callback = $this->getOption('callback');

        if (is_callable($callback)) {
            $validateValue = call_user_func($callback, $value);
            if (is_bool($validateValue)) {
                if (!$validateValue) {
                    $message = $this->getOption("message");

                    if (!$message) {
                        $message = "The Callback value is not valid";
                    }

                    $validation->appendMessage(
                        new Message($message, $field, "Ip")
                    );

                    return false;
                }
                return true;
            } elseif (is_object($validateValue) && $validateValue instanceof Validator) {
                return $validateValue->validate($validation, $field);
            }
            throw new Exception('Callback must return boolean or Phalcon\\Validation\\Validator object');
        }

        return true;
    }
}
