<?php
/**
 * Ip Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.0
 * @package Itdashu
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Ip
 *
 * Checks if a value has a correct e-mail format
 *
 *<code>
 *use Itdashu\Validation\Validator\Ip as IpValidator;
 *
 *$validation->add('ip', new IpValidator(array(
 *   'message' => 'The e-mail is not valid'
 *)));
 *</code>
 *
 */
class Ip extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        $value = $validation->getValue($field);

        if (!filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_IPV6)) {
            $message = $this->getOption("message");

            if (!$message) {
                $message = "The IP is not valid";
            }

            $validation->appendMessage(
                new Message($message, $field, "Ip")
            );

            return false;
        }

        return true;
    }
}
