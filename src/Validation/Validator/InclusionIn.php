<?php
/**
 * InclusionIn Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.0
 * @package Itdashu
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\InclusionIn
 *
 * Check if a value is included into a list of values
 *
 *<code>
 *use \Itdashu\Easy\Validation\Validator\InclusionIn;
 *
 *$validation->add('status', new InclusionIn(array(
 *   'message' => 'The status must be A or B',
 *   'domain' => array('A', 'B')
 *)));
 *</code>
 *
 * @see https://github.com/phalcon/cphalcon/blob/1.2.6/ext/validation/validator/inclusionin.c
 */
class InclusionIn extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        if (is_object($validation) === false ||
            $validation instanceof Validation === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_string($field) === false) {
            throw new Exception('Invalid parameter type.');
        }

        $value = $validation->getValue($field);

        //A domain is an array with a list of valid values
        $domain = $this->getOption('domain');

        if (is_array($domain) === false) {
            throw new Exception("Option 'domain' must be an array");
        }

        //Check if the value is contained by the array
        if (in_array($value, $domain) === false) {
            $message = $this->getOption('message');
            if (empty($message) === true) {
                $message = "Value of field '" . $field . "' must be part of list: " . implode(', ', $domain);
            }

            $validation->appendMessage(new Message($message, $field, 'InclusionIn'));

            return false;
        }

        return true;
    }
}
