<?php
/**
 * Digit Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.0
 * @package Itdashu
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Digit
 *
 * Checks if a value has a correct e-mail format
 *
 * <code>
 * use Phalcon\Validation;
 * use Phalcon\Validation\Validator\Digit as DigitValidator;
 *
 * $validator = new Validation();
 *
 * $validator->add(
 *     "height",
 *     new DigitValidator(
 *         [
 *             "message" => ":field must be numeric",
 *         ]
 *     )
 * );
 *
 * $validator->add(
 *     [
 *         "height",
 *         "width",
 *     ],
 *     new DigitValidator(
 *         [
 *             "message" => [
 *                 "height" => "height must be numeric",
 *                 "width"  => "width must be numeric",
 *             ],
 *         ]
 *     )
 * );
 * </code>
 *
 */
class Digit extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        $value = $validation->getValue($field);

        if (is_int($value) || ctype_digit($value)) {
            return true;
        }

        $message = $this->getOption("message");

        if (!$message) {
            $message = "The Digit value is not valid";
        }

        $validation->appendMessage(
            new Message($message, $field, "Ip")
        );

        return false;
    }
}
