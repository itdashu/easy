<?php
/**
 * Regex Validator
 *
 * @author Andres Gutierrez <andres@phalconphp.com>
 * @author Eduar Carvajal <eduar@phalconphp.com>
 * @author Wenzel Pünter <wenzel@phelix.me>
 * @version 1.2.6
 * @package Phalcon
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Regex
 *
 * Allows validate if the value of a field matches a regular expression
 *
 *<code>
 *use Phalcon\Validation\Validator\Regex as RegexValidator;
 *
 *$validation->add('created_at', new RegexValidator(array(
 *   'pattern' => '/^[0-9]{4}[-\/](0[1-9]|1[12])[-\/](0[1-9]|[12][0-9]|3[01])$/',
 *   'message' => 'The creation date is invalid'
 *)));
 *</code>
 *
 * @see https://github.com/phalcon/cphalcon/blob/1.2.6/ext/validation/validator/regex.c
 */
class Regex extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        if (is_object($validation) === false ||
            $validation instanceof Validation === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_string($field) === false) {
            throw new Exception('Invalid parameter type.');
        }

        $value = $validation->getValue($field);

        //the regular expression is set in the option 'pattern'
        $pattern = $this->getOption('pattern');

        //Check if the value matches using preg_match in the PHP userland
        $matchPattern = preg_match($pattern, $value, $matches);

        if ($matchPattern == true) {
            $failed = ($matches[0] !== $value);
        } else {
            $failed = true;
        }

        if ($failed === true) {
            //Check if the developer has defined a custom message
            $message = $this->getOption('message');

            if (empty($message) === true) {
                $message = "Value of field '" . $field . "' doesn't match regular expression";
            }

            $validation->appendMessage(new Message($message, $field, 'Regex'));

            return false;
        }

        return true;
    }
}
