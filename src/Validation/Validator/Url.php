<?php
/**
 * Url Validator
 *
 * @author Andres Gutierrez <andres@Itdashuphp.com>
 * @author Eduar Carvajal <eduar@Itdashuphp.com>
 * @author Wenzel Pünter <wenzel@phelix.me>
 * @version 1.2.6
 * @package Itdashu
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Url
 *
 * Checks if a value has a correct e-mail format
 *
 *<code>
 *use Itdashu\Validation\Validator\Url as UrlValidator;
 *
 *$validation->add('ip', new UrlValidator(array(
 *   'message' => 'The e-mail is not valid'
 *)));
 *</code>
 *
 */
class Url extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        $value = $validation->getValue($field);

        if (!filter_var($value, FILTER_VALIDATE_URL)) {
            $message = $this->getOption("message");

            if (!$message) {
                $message = "The Url value is not valid";
            }

            $validation->appendMessage(
                new Message($message, $field, "Ip")
            );

            return false;
        }

        return true;
    }
}
