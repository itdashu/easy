<?php
/**
 * CreditCard Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.2.6
 * @package Itdashu
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\CreditCard
 *
 * Checks if a value has a correct e-mail format
 *
 * <code>
 * use \Itdashu\Easy\Validation;
 * use \Itdashu\Easy\Validation\Validator\CreditCard as CreditCardValidator;
 *
 * $validator = new Validation();
 *
 * $validator->add(
 *     "creditCard",
 *     new CreditCardValidator(
 *         [
 *             "message" => "The credit card number is not valid",
 *         ]
 *     )
 * );
 *
 * $validator->add(
 *     [
 *         "creditCard",
 *         "secondCreditCard",
 *     ],
 *     new CreditCardValidator(
 *         [
 *             "message" => [
 *                 "creditCard"       => "The credit card number is not valid",
 *                 "secondCreditCard" => "The second credit card number is not valid",
 *             ],
 *         ]
 *     )
 * );
 * </code>
 *
 */
class CreditCard extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        $value = $validation->getValue($field);

        $valid = $this->verifyByLuhnAlgorithm($value);


        if (!$valid) {
            $message = $this->getOption("message");

            if (!$message) {
                $message = "The CreditCard value is not valid";
            }

            $validation->appendMessage(
                new Message($message, $field, "Ip")
            );

            return false;
        }

        return true;
    }

    /**
     * @param string $number
     * @return bool
     */
    private function verifyByLuhnAlgorithm(string $number): bool
    {
        $digits = (array)str_split($number);
        $hash = '';
        foreach (array_reverse($digits) as $position => $digit) {
            $hash .= ($position % 2 ? $digit * 2 : $digit);
        }
        $result = array_sum(str_split($hash));

        return ($result % 10 == 0);
    }
}
