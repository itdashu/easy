<?php
/**
 * Email Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.2.6
 * @package Itdashu
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * Itdashu\Easy\Validation\Validator\File
 *
 * Checks if a value has a correct file
 *
 * <code>
 * use Itdashu\Easy\Validation;
 * use Itdashu\Easy\Validation\Validator\File as FileValidator;
 *
 * $validation = new Validation();
 *
 * $validation->add(
 *     "file",
 *     new FileValidator(
 *         [
 *             "maxSize"              => "2M",
 *             "messageSize"          => ":field exceeds the max filesize (:max)",
 *             "allowedTypes"         => [
 *                 "image/jpeg",
 *                 "image/png",
 *             ],
 *             "messageType"          => "Allowed file types are :types",
 *             "maxResolution"        => "800x600",
 *             "messageMaxResolution" => "Max resolution of :field is :max",
 *         ]
 *     )
 * );
 *
 * $validation->add(
 *     [
 *         "file",
 *         "anotherFile",
 *     ],
 *     new FileValidator(
 *         [
 *             "maxSize" => [
 *                 "file"        => "2M",
 *                 "anotherFile" => "4M",
 *             ],
 *             "messageSize" => [
 *                 "file"        => "file exceeds the max filesize 2M",
 *                 "anotherFile" => "anotherFile exceeds the max filesize 4M",
 *             "allowedTypes" => [
 *                 "file"        => [
 *                     "image/jpeg",
 *                     "image/png",
 *                 ],
 *                 "anotherFile" => [
 *                     "image/gif",
 *                     "image/bmp",
 *                 ],
 *             ],
 *             "messageType" => [
 *                 "file"        => "Allowed file types are image/jpeg and image/png",
 *                 "anotherFile" => "Allowed file types are image/gif and image/bmp",
 *             ],
 *             "maxResolution" => [
 *                 "file"        => "800x600",
 *                 "anotherFile" => "1024x768",
 *             ],
 *             "messageMaxResolution" => [
 *                 "file"        => "Max resolution of file is 800x600",
 *                 "anotherFile" => "Max resolution of file is 1024x768",
 *             ],
 *         ]
 *     )
 * );
 * </code>
 */
class File extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        $value = $validation->getValue($field);
        if (is_string($value)) {
            $valueInfo = [$value];
        } else {
            $valueInfo = $value;
        }

        if (!is_array($valueInfo)) {
            throw new Exception('file value should is array or string');
        }

        $byteUnits = ["B" => 0, "K" => 10, "M" => 20, "G" => 30, "T" => 40, "KB" => 10, "MB" => 20, "GB" => 30, "TB" => 40];

        foreach ($valueInfo as $fileValue) {
            if ($this->hasOption('maxSize')) {
                $maxSize = $this->getOption("maxSize");
                $matches = null;
                $unit = "B";

                preg_match("/^([0-9]+(?:\\.[0-9]+)?)(" . implode("|", array_keys($byteUnits)) . ")?$/Di", $maxSize, $matches);

                if (isset($matches[2])) {
                    $unit = $matches[2];
                }

                $bytes = floatval($matches[1]) * pow(2, $byteUnits[$unit]);
                $fileSize = filesize($fileValue);
                if (floatval($fileSize) > floatval($bytes)) {
                    $message = $this->getOption('message');
                    if (empty($message) === true) {
                        $message = "Value of field '" . $field . "' must have a valid e-mail format";
                    }

                    $validation->appendMessage(new Message($message, $field, 'Email'));

                    return false;
                }
            }

            if ($this->hasOption("allowedTypes")) {

                $fieldTypes = $this->getOption("allowedTypes");
                if (is_string($fieldTypes)) {
                    $fieldTypes = [$fieldTypes];
                }
                if (!is_array($fieldTypes)) {
                    throw new Exception('allowedTypes options should is array!');
                }

                if (function_exists("finfo_open")) {
                    $tmp = finfo_open(FILEINFO_MIME_TYPE);
                    $mime = finfo_file($tmp, $fileValue);

                    finfo_close($tmp);
                } else {
                    throw new Exception('finfo_open function not found!');
                }

                if (!in_array($mime, $fieldTypes)) {
                    $message = $this->getOption('message');
                    if (empty($message) === true) {
                        $message = "the file type '" . $field . "' not allow, allow type: " . implode('|', $fieldTypes);
                    }

                    $validation->appendMessage(new Message($message, $field, 'FileType'));

                    return false;
                }
            }

            if ($this->hasOption("minResolution") || $this->hasOption("maxResolution")) {
                $tmp = getimagesize($fileValue);
                $width = $tmp[0];
                $height = $tmp[1];

                if ($this->hasOption("minResolution")) {
                    $minResolution = $this->getOption("minResolution");
                    if (is_array($minResolution)) {
                        $minResolution = $minResolution[$field];
                    }
                    $minResolutionArray = explode("x", $minResolution);
                    $minWidth = $minResolutionArray[0];
                    $minHeight = $minResolutionArray[1];
                } else {
                    $minWidth = 1;
                    $minHeight = 1;
                }

                if ($width < $minWidth || $height < $minHeight) {

                    $message = $this->getOption('message');
                    if (empty($message) === true) {
                        $message = "file size is small!";
                    }

                    $validation->appendMessage(new Message($message, $field, 'FileSize'));

                    return false;
                }

                if ($this->hasOption("maxResolution")) {

                    $maxResolution = $this->getOption("maxResolution");
                    if (is_array($maxResolution)) {
                        $maxResolution = $maxResolution[$field];
                    }
                    $maxResolutionArray = explode("x", $maxResolution);
                    $maxWidth = $maxResolutionArray[0];
                    $maxHeight = $maxResolutionArray[1];

                    if ($width > $maxWidth || $height > $maxHeight) {

                        $message = $this->getOption('message');
                        if (empty($message) === true) {
                            $message = "file size is big!";
                        }

                        $validation->appendMessage(new Message($message, $field, 'FileSize'));

                        return false;
                    }
                }
            }

        }
        return true;
    }

    /**
     * @param Validation $validation
     * @param string $field
     * @return bool
     * @throws Exception
     */
    public function isAllowEmpty(Validation $validation, string $field)
    {
        $value = $validation->getValue($field);
        return empty($value) || isset($value["error"]) && $value["error"] === UPLOAD_ERR_NO_FILE;
    }
}
