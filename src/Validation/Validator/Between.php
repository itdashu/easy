<?php
/**
 * Between Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.0
 * @package Phalcon
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use \Exception;
use Itdashu\Easy\Validation\Message;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Between
 *
 * Validates that a value is between a range of two values
 *
 *<code>
 *use Phalcon\Validation\Validator\Between;
 *
 *$validation->add('name', new Between(array(
 *   'minimum' => 0,
 *   'maximum' => 100,
 *   'message' => 'The price must be between 0 and 100'
 *)));
 *</code>
 *
 */
class Between extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        $value = $validation->getValue($field);
        $minimum = $this->getOption('minimum');
        $maximum = $this->getOption('maximum');

        if ($value <= $minimum || $value >= $maximum) {
            $messageStr = $this->getOption('message');
            if (empty($messageStr) === true) {
                $messageStr = $field . ' is not between a valid range';
            }

            $validation->appendMessage(new Message($messageStr, $field, 'Between'));

            return false;
        }

        return true;
    }
}
