<?php
/**
 * Numericality Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.0
 * @package Itdashu
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Numericality
 *
 * Checks if a value has a correct e-mail format
 *
 * <code>
 * use Phalcon\Validation;
 * use Phalcon\Validation\Validator\Numericality;
 *
 * $validator = new Validation();
 *
 * $validator->add(
 *     "price",
 *     new Numericality(
 *         [
 *             "message" => ":field is not numeric",
 *         ]
 *     )
 * );
 *
 * $validator->add(
 *     [
 *         "price",
 *         "amount",
 *     ],
 *     new Numericality(
 *         [
 *             "message" => [
 *                 "price"  => "price is not numeric",
 *                 "amount" => "amount is not numeric",
 *             ]
 *         ]
 *     )
 * );
 * </code>
 */
class Numericality extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        $value = $validation->getValue($field);

        if (!preg_match("/^\d+\.?\d*$/", $value)) {
            $message = $this->getOption("message");

            if (!$message) {
                $message = "The Numericality value is not valid";
            }

            $validation->appendMessage(
                new Message($message, $field, "Ip")
            );

            return false;
        }

        return true;
    }
}
