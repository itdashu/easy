<?php
/**
 * Email Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.2.6
 * @package Itdashu
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Email
 *
 * Checks if a value has a correct e-mail format
 *
 *<code>
 *use \Itdashu\Easy\Validation\Validator\Email as EmailValidator;
 *
 *$validation->add('email', new EmailValidator(array(
 *   'message' => 'The e-mail is not valid'
 *)));
 *</code>
 *
 */
class Email extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        $value = $validation->getValue($field);
        if (filter_var($value, \FILTER_VALIDATE_EMAIL) === false) {
            $message = $this->getOption('message');
            if (empty($message) === true) {
                $message = "Value of field '" . $field . "' must have a valid e-mail format";
            }

            $validation->appendMessage(new Message($message, $field, 'Email'));

            return false;
        }

        return true;
    }
}
