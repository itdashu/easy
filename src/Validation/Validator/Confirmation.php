<?php
/**
 * Confirmation Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.0
 * @package Phalcon
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use \Exception;
use Itdashu\Easy\Validation\Message;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Confirmation
 *
 * Checks that two values have the same value
 *
 *<code>
 *use \Itdashu\Easy\Validation\Validator\Confirmation;
 *
 *$validation->add('password', new Confirmation(array(
 *   'message' => 'Password doesn\'t match confirmation',
 *   'with' => 'confirmPassword'
 *)));
 *</code>
 *
 */
class Confirmation extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {
        if (is_object($validation) === false ||
            $validation instanceof Validation === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_string($field) === false) {
            throw new Exception('Invalid parameter type.');
        }

        $withAttribute = $this->getOption('with');
        $value = $validation->getValue($field);
        if ($value !== $withAttribute) {
            $message = $this->getOption('message');
            if (empty($message) === true) {
                $message = 'Value of \'' . $field . '\' and \'' . $withAttribute . '\' don\'t match';
            }

            $validation->appendMessage(new Message($message, $field, 'Confirmation'));
            return false;
        }

        return true;
    }
}
