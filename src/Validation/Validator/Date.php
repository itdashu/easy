<?php
/**
 * Date Validator
 *
 * @author Leno <leno@itdashu.com>
 * @version 1.0
 * @package Itdashu
 */

namespace Itdashu\Easy\Validation\Validator;

use Itdashu\Easy\Validation\Validator;
use Itdashu\Easy\Validation\ValidatorInterface;
use Itdashu\Easy\Validation\Message;
use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator\Email
 *
 * Checks if a value has a correct e-mail format
 *
 *<code>
 *use Itdashu\Validation\Validator\Email as EmailValidator;
 *
 *$validation->add('ip', new EmailValidator(array(
 *   'message' => 'The e-mail is not valid'
 *)));
 *</code>
 *
 */
class Date extends Validator implements ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validation
     * @param string $field
     * @return boolean
     * @throws Exception
     */
    public function validate(Validation $validation, string $field): bool
    {

        $value = $validation->getValue($field);
        $format = $this->getOption('format');

        if (is_array($format)) {
            $format = $format[$field];
        }

        if (empty($format)) {
            $format = 'Y-m-d';
        }

        if (!$this->checkDate($value, $format)) {
            $message = $this->getOption("message");

            if (!$message) {
                $message = "The date value is not valid";
            }

            $validation->appendMessage(
                new Message($message, $field, "Ip")
            );

            return false;
        }

        return true;
    }

    /**
     * validate date value
     * @param string $value
     * @param string $format
     * @return bool
     */
    private function checkDate(string $value, string $format): bool
    {
        $date = \DateTime::createFromFormat($format, $value);
        $errors = \DateTime::getLastErrors();
        unset($date);
        if ($errors["warning_count"] > 0 || $errors["error_count"] > 0) {
            return false;
        }
        return true;
    }
}
