<?php
/**
 * Group
 *
 * @author Andres Gutierrez <andres@phalconphp.com>
 * @author Eduar Carvajal <eduar@phalconphp.com>
 * @author Wenzel Pünter <wenzel@phelix.me>
 * @version 1.2.6
 * @package Phalcon
 */

namespace Itdashu\Easy\Validation\Message;

use ArrayAccess;
use Countable;
use Exception;
use Itdashu\Easy\Validation\Message;
use Iterator;

/**
 * \Itdashu\Easy\Validation\Message\Group
 *
 * Represents a group of validation messages
 */
class Group implements Countable, ArrayAccess, Iterator
{
    /**
     * Position
     *
     * @var null|int
     * @access protected
     */
    protected $_position;

    /**
     * Messages
     *
     * @var null|array
     * @access protected
     */
    protected $_messages;

    /**
     * Group constructor.
     * @param array|null $messages
     */
    public function __construct(array $messages = null)
    {
        if (is_array($messages) === true) {
            $this->_messages = $messages;
        } else {
            $this->_messages = array();
        }
    }

    /**
     * Magic __set_state helps to re-build messages variable when exporting
     * @param $group
     * @return Group
     * @throws Exception
     */
    public static function __set_state($group): Group
    {
        if (is_array($group) === false) {
            throw new Exception('Invalid parameter type.');
        }

        return new Group($group['_messages']);
    }

    /**
     * Gets an attribute a message using the array syntax
     *
     *<code>
     * print_r($messages[0]);
     *</code>
     *
     * @param mixed $index
     * @return \Itdashu\Easy\Validation\Message|null
     * @throws Exception
     */
    public function offsetGet($index)
    {
        if (is_scalar($index) === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (isset($this->_messages[$index]) === true) {
            return $this->_messages[$index];
        }

        return null;
    }

    /**
     * Sets an attribute using the array-syntax
     *
     *<code>
     * $messages[0] = new \Itdashu\Easy\Validation\Message('This is a message');
     *</code>
     *
     * @param mixed $index
     * @param \Itdashu\Easy\Validation\Message $message
     * @throws Exception
     */
    public function offsetSet($index, $message)
    {
        if (is_scalar($index) === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_object($message) === false &&
            $message instanceof Message === false) {
            throw new Exception('The message must be an object');
        }

        $this->_messages[$index] = $message;
    }

    /**
     * Checks if an index exists
     *
     *<code>
     * var_dump(isset($message['database']));
     *</code>
     *
     * @param mixed $index
     * @return boolean
     * @throws Exception
     */
    public function offsetExists($index)
    {
        if (is_scalar($index) === false) {
            throw new Exception('Invalid parameter type.');
        }

        return isset($this->_messages[$index]);
    }

    /**
     * Removes a message from the list
     *
     *<code>
     * unset($message['database']);
     *</code>
     *
     * @param mixed $index
     * @throws Exception
     */
    public function offsetUnset($index)
    {
        if (is_scalar($index) === false) {
            throw new Exception('Invalid parameter type.');
        }

        unset($this->_messages[$index]);
    }

    /**
     * Appends an array of messages to the group
     *
     *<code>
     * $messages->appendMessages($messagesArray);
     *</code>
     *
     * @param Message[]|array $messages
     * @throws Exception
     */
    public function appendMessages($messages)
    {
        if (is_array($messages) === false) {
            if (is_object($messages) === false ||
                $messages instanceof Message === false) {
                throw new Exception('The message must be array or object');
            }
        }

        if (is_array($messages) === true) {
            //An array of messages is simply merged into the current one
            $this->_messages = array_merge($this->_messages, $messages);
        } else {
            //A group of messages is iterated and appended one-by-one to the current list
            /** @var Group $messages */
            $messages->rewind();

            while ($messages->valid() !== false) {
                $this->appendMessage($messages->current());
                $messages->next();
            }
        }
    }

    /**
     * Rewinds the internal iterator
     */
    public function rewind()
    {
        $this->_position = 0;
    }

    /**
     * Check if the current message in the iterator is valid
     *
     * @return boolean
     */
    public function valid(): bool
    {
        return isset($this->_messages[$this->_position]);
    }

    /**
     * Appends a message to the group
     * @param Message $message
     * @return Group
     *<code>
     * $messages->appendMessage(new \Itdashu\Easy\Validation\Message('This is a message'));
     *</code>
     */
    public function appendMessage(Message $message): Group
    {
        $this->_messages[] = $message;
        return $this;
    }

    /**
     * Returns the current message in the iterator
     *
     * @return Message|null
     */
    public function current()
    {
        if (isset($this->_messages[$this->_position]) === true) {
            return $this->_messages[$this->_position];
        }

        return null;
    }

    /**
     * Moves the internal iteration pointer to the next position
     */
    public function next()
    {
        ++$this->_position;
    }

    /**
     * Filters the message group by field name
     *
     * @param string $fieldName
     * @return array
     * @throws Exception
     */
    public function filter(string $fieldName): array
    {
        if (is_string($fieldName) === false) {
            throw new Exception('Invalid parameter type.');
        }

        $filtered = array();

        if (empty($this->_messages) === false) {
            foreach ($this->_messages as $message) {
                if (method_exists($message, 'getField') === true) {
                    if ($fieldName === $message->getField()) {
                        $filtered[] = $message;
                    }
                }
            }
        }

        return $filtered;
    }

    /**
     * Returns the current position/key in the iterator
     *
     * @return int|null
     */
    public function key()
    {
        return $this->_position;
    }

    /**
     * 格式化错误信息
     * @return array
     */
    public function toArray()
    {
        $output = [];
        /** @var Message $message */
        foreach ($this->_messages as $message) {
            $output[$message->getField()][] = $message->getMessage();
        }
        return $output;
    }

    /**
     * 检测是否验证通过
     * @return bool
     */
    public function toBool()
    {
        return $this->count() <= 0;
    }
    

    /**
     * Returns the number of messages in the list
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->_messages);
    }
}
