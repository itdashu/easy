<?php
/**
 * Validator
 *
 * @author Andres Gutierrez <andres@phalconphp.com>
 * @author Eduar Carvajal <eduar@phalconphp.com>
 * @author Wenzel Pünter <wenzel@phelix.me>
 * @version 1.2.6
 * @package Phalcon
 */

namespace Itdashu\Easy\Validation;

use \Exception;
use Itdashu\Easy\Validation;

/**
 * \Itdashu\Easy\Validation\Validator
 *
 * This is a base class for validators
 */
abstract class Validator
{
    /**
     * Options
     *
     * @var null
     * @access protected
     */
    protected $_options;

    /**
     * \Itdashu\Easy\Validation\Validator constructor
     *
     * @param array|null $options
     * @throws Exception
     */
    public function __construct(array $options = null)
    {
        if (is_array($options) === true) {
            $this->_options = $options;
        } elseif (is_null($options) === false) {
            //@note this exception message is nonsence
            throw new Exception('The attribute must be a string');
        }
    }

    /**
     * Checks if an option is defined
     *
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public function isSetOption(string $key): bool
    {
        if (is_string($key) === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_array($this->_options) === true) {
            return isset($this->_options[$key]);
        }

        return false;
    }

    /**
     * Returns an option in the validator's options
     * Returns null if the option hasn't been set
     *
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public function getOption(string $key)
    {
        if (is_string($key) === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_array($this->_options) === true) {
            if (isset($this->_options[$key]) === true) {
                return $this->_options[$key];
            }
        }

        return null;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasOption(string $key): bool
    {
        return isset($this->_options[$key]);
    }

    /**
     * Sets an option in the validator
     * @param string $key
     * @param $value
     * @return Validator
     * @throws Exception
     */
    public function setOption(string $key, $value): Validator
    {
        if (is_string($key) === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_array($this->_options) === false) {
            $this->_options = array();
        }

        $this->_options[$key] = $value;
        return $this;
    }

    /**
     * Executes the validation
     * @param Validation $validation
     * @param string $attribute
     * @return bool
     */
    abstract public function validate(Validation $validation, string $attribute) : bool ;
}
