<?php
/**
 * Message
 *
 * @author Andres Gutierrez <andres@phalconphp.com>
 * @author Eduar Carvajal <eduar@phalconphp.com>
 * @author Wenzel Pünter <wenzel@phelix.me>
 * @version 1.2.6
 * @package Phalcon
*/
namespace Itdashu\Easy\Validation;

use \Exception;

/**
 * Itdashu\Easy\Validation
 * Encapsulates validation info generated in the validation process
 */
class Message
{
    /**
     * Type
     *
     * @var null|string
     * @access protected
    */
    protected $_type;

    /**
     * Message
     *
     * @var null|string
     * @access protected
    */
    protected $_message;

    /**
     * Field
     *
     * @var null|string
     * @access protected
    */
    protected $_field;

    /**
     * Message constructor.
     * @param string $message
     * @param string|null $field
     * @param string|null $type
     * @throws Exception
     */
    public function __construct(string $message, string $field = null, string $type = null)
    {
        if (is_string($message) === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_string($field) === false && is_null($field) === false) {
            throw new Exception('Invalid parameter type.');
        }

        if (is_string($type) === false && is_null($type) === false) {
            throw new Exception('Invalid parameter type.');
        }

        $this->_message = $message;
        $this->_field = $field;
        $this->_type = $type;
    }

    /**
     * @param string $type
     * @return Message
     * @throws Exception
     */
    public function setType(string $type):Message
    {
        $this->_type = $type;
        return $this;
    }

    /**
     * Returns message type
     *
     * @return string|null
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * Sets verbose message
     * @param string $message
     * @return Message
     */
    public function setMessage(string $message):Message
    {
        $this->_message = $message;
        return $this;
    }

    /**
     * Returns verbose message
     *
     * @return string
     */
    public function getMessage():string
    {
        return $this->_message;
    }

    /**
     * Sets field name related to message
     * @param string $field
     * @return Message
     * @throws Exception
     */
    public function setField(string $field):Message
    {
        if (is_string($field) === false) {
            throw new Exception('Invalid parameter type.');
        }

        $this->_field = $field;
        return $this;
    }

    /**
     * Returns field name related to message
     *
     * @return string|null
     */
    public function getField()
    {
        return $this->_field;
    }

    /**
     * Magic __toString method returns verbose message
     *
     * @return string
     */
    public function __toString():string
    {
        return $this->_message;
    }

    /**
     * Magic __set_state helps to recover messsages from serialization
     * @param array $message
     * @return Message
     * @throws Exception
     */
    public static function __set_state(array $message):Message
    {
        return new Message($message['_message'], $message['_field'], $message['_type']);
    }
}
