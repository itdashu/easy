<?php
/**
 * Validator Interface
 *
 * @author Andres Gutierrez <andres@phalconphp.com>
 * @author Eduar Carvajal <eduar@phalconphp.com>
 * @version 1.2.6
 * @package Phalcon
*/
namespace Itdashu\Easy\Validation;

use Itdashu\Easy\Validation;

interface ValidatorInterface
{
    /**
     * Checks if an option is defined
     *
     * @param string $key
     * @return mixed
     */
    public function isSetOption(string $key);

    /**
     * Returns an option in the validator's options
     * Returns null if the option hasn't been set
     *
     * @param string $key
     * @return mixed
     */
    public function getOption(string $key);

    /**
     * Executes the validation
     *
     * @param \Itdashu\Easy\Validation $validator
     * @param string $attribute
     * @return \Itdashu\Easy\Validation\Message\Group
     */
    public function validate(Validation $validator, string $attribute);
}
