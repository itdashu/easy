<?php

namespace Itdashu\Easy;

class Config
{
    public static function loadDir(string $dir)
    {
        $output = [];
        $configList = scandir($dir);
        foreach ($configList as $config) {
            $configInfo = pathinfo($config);
            if ($configInfo['extension'] === 'php') {
                $output[$configInfo['filename']] = require $dir . $config;
            }
        }
        return $output;
    }

    public static function set(string $name, array $value, string $dir)
    {
        $fileName = $dir . $name . '.php';
        $valueContent = var_export($value, true);
        $content = "<?php return {$valueContent};";
        return file_put_contents($fileName, $content);
    }
}