<?php
/**
 * This file is part of HongWu soft.
 *
 * @link     https://www.hongwu.info
 * @document https://doc.hongwu.info
 * @contact  leno@itdashu.com
 * @license  https://www.hongwu.info/license.html
 */

namespace Itdashu\Easy;

use \RuntimeException;
use \Closure;

/**
 * \Itdashu\Easy\Filter
 *
 * The Itdashu\Easy\Filter component provides a set of commonly needed data filters. It provides
 * object oriented wrappers to the php filter extension. Also allows the developer to
 * define his/her own filters
 *
 *<code>
 *  $filter = new Itdashu\Easy\Filter();
 *  $filter->sanitize("some(one)@exa\\mple.com", "email"); // returns "someone@example.com"
 *  $filter->sanitize("hello<<", "string"); // returns "hello"
 *  $filter->sanitize("!100a019", "int"); // returns "100019"
 *  $filter->sanitize("!100a019.01a", "float"); // returns "100019.01"
 *</code>
 *
 * @see https://github.com/phalcon/cphalcon/blob/1.2.6/ext/filter.c
 */
class Filter
{
    /**
     * Filters
     *
     * @var null|array
     * @access protected
     */
    protected $_filters = null;

    /**
     * 唯一实例
     * @var Filter
     */
    public static $instance;

    protected $validation = null;

    public function __construct()
    {
        $this->validation = new Validation();
    }

    /**
     * Adds a user-defined filter
     * @param string $name
     * @param callable $handler
     * @return Filter
     * @throws RuntimeException
     */
    public function add(string $name, $handler): Filter
    {
        if (is_string($name) === false) {
            throw new RuntimeException('Filter name must be string');
        }

        if (is_object($handler) === false) {
            throw new RuntimeException('Filter must be an object');
        }

        if (is_array($this->_filters) === false) {
            $this->_filters = [];
        }

        $this->_filters[$name] = $handler;
        return $this;
    }

    /**
     * Sanitizes a value with a specified single or set of filters
     * @param $value
     * @param $filters
     * @param null $name
     * @return array|mixed|string
     */
    public function sanitize($value, $filters, $name = null)
    {
        //Apply an array of filters
        if (is_array($filters) === true) {
            if (is_null($value) === false) {
                foreach ($filters as $filter) {
                    if (is_array($value) === true) {
                        $arrayValue = [];

                        foreach ($value as $itemKey => $itemValue) {
                            //@note no type check of $itemKey
                            $arrayValue[$itemKey] = $this->_sanitize($itemValue, $filter, $name);
                        }

                        $value = $arrayValue;
                    } else {
                        $value = $this->_sanitize($value, $filter, $name);
                    }
                }
            }

            return $value;
        }

        //Apply a single filter value
        if (is_array($value) === true) {
            $sanitizedValue = [];
            foreach ($value as $key => $itemValue) {
                //@note no type check of $key
                $sanitizedValue[$key] = $this->_sanitize($itemValue, $filters, $name);
            }
        } else {
            $sanitizedValue = $this->_sanitize($value, $filters, $name);
        }

        return $sanitizedValue;
    }

    /**
     * Internal sanitize wrapper to filter_var
     *
     * @param $value
     * @param string $filter
     * @param null $name
     * @return mixed|string
     */
    protected function _sanitize($value, string $filter, $name = null)
    {
        if (is_string($filter) === false) {
            throw new RuntimeException('Invalid parameter type.');
        }
        $filters = $this->_filters;
        /* User-defined filter */
        if (isset($filters[$filter]) == true) {
            $filterObject = $this->_filters[$filter];
            if ($filterObject instanceof Closure) {
                return call_user_func_array($filterObject, [$value]);
            }

            return $filterObject->filter($value);
        }

        switch ($name) {
            case 'page':
                $this->validation->addByConfig($name, [
                    'type' => 'regex',
                    'pattern' => '|^[1-9][0-9]*$|'
                ]);
                break;
            case 'size':
                $this->validation->addByConfig($name, [
                    'type' => 'regex',
                    'pattern' => '|^[1-9][0-9]*$|'
                ]);
                $this->validation->addByConfig($name, [
                    'type' => 'between',
                    'minimum' => 1,
                    'maximum' => 50
                ]);
                break;
            case 'order':
                $this->validation->addByConfig($name, [
                    'type' => 'regex',
                    'pattern' => '|^[a-zA-Z ]*?$|'
                ]);
                break;
        }

        /* Predefined filter */
        switch ($filter) {
            case 'email':
                $this->validation->addByConfig($name, [
                    'type' => 'email'
                ]);
                return filter_var(str_replace('\'', '', $value), FILTER_SANITIZE_EMAIL);
                break;
            case 'int':
                //过滤为整数
                $this->validation->addByConfig($name, [
                    'type' => 'regex',
                    'pattern' => '|^[0-9]{1,}$|'
                ]);
                $value = filter_var($value, FILTER_SANITIZE_NUMBER_INT);
                return str_replace(['+', '-'], '', $value);
                break;
            case 'url':
                $this->validation->addByConfig($name, [
                    'type' => 'url'
                ]);
                return filter_var($value, FILTER_SANITIZE_URL);
                break;
            case 'string':
                return filter_var($value, FILTER_SANITIZE_STRING);
                break;
            case 'float':
                $this->validation->addByConfig($name, [
                    'type' => 'regex',
                    'pattern' => '|^\d+\.?\d{1,}$|'
                ]);
                return filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, ['flags' => 4096]);
                break;
            case 'alphanum':
                $this->validation->addByConfig($name, [
                    'type' => 'alnum'
                ]);
                $filtered = '';
                $value = (string)$value;
                $valueLength = strlen($value);
                $zeroChar = chr(0);

                for ($i = 0; $i < $valueLength; ++$i) {
                    if ($value[$i] == $zeroChar) {
                        break;
                    }

                    if (ctype_alnum($value[$i]) === true) {
                        $filtered .= $value[$i];
                    }
                }
                return $filtered;
                break;
            case 'trim':
                return trim($value);
                break;
            case 'striptags':
                return strip_tags($value);
                break;
            case 'lower':
                if (function_exists('mb_strtolower') === true) {
                    return mb_strtolower($value);
                } else {
                    //@note we use the default implementation instad of a custom one
                    return strtolower($value);
                }
                break;
            case 'upper':
                if (function_exists('mb_strtoupper') === true) {
                    return mb_strtoupper($value);
                } else {
                    //@note we use the default implementation instad of a custom one
                    return strtoupper($value);
                }
                break;
            default:
                if (function_exists($filter) === true) {
                    $this->validation->addByConfig($name, [
                        'type' => 'callback',
                        'callback' => $filter
                    ]);
                    return $filter($value);
                } else {
                    if (Str::startWith($filter, '!')) {
                        $filter = ltrim($filter, '!');
                    }

                    $this->validation->addByConfig($name, [
                        'type' => 'regex',
                        'pattern' => $filter
                    ]);
                }

                return $value;
                break;
        }
    }

    /**
     * Return the user-defined filters in the instance
     *
     * @return array
     */
    public function getFilters(): array
    {
        return $this->_filters;
    }

    /**
     * 过滤数据
     * @param array $filters 过滤器
     * @param array $data 需要过滤的数据
     * @param array $default 默认填充数据
     * @return array
     */
    public static function handle(array $filters, array $data, $default = [], $loose = false): array
    {
        $output = [];
        $filter = new Filter();
        $oldFilters = $filters;
        try {
            foreach ($filters as $name => $item) {
                if (Str::startWith($item, '!') && !isset($data[$name])) {
                    throw new RuntimeException('参数错误！');
                }
                if (!$loose && isset($data[$name])) {
                    $output[$name] = $filter->sanitize($data[$name], $item, $name);
                }
            }
            if ($loose) {
                foreach ($data as $name => $item) {
                    if (is_string($item) && !isset($filters[$name]) && $loose) {
                        $filters[$name] = 'string';
                    }else{
                      $output[$name] = $item;
                      continue;
                    }

                    $output[$name] = $filter->sanitize($item, $filters[$name], $name);
                }
            }
        } catch (RuntimeException $e) {
            $output = [];
        }

        $output = array_merge($default, $output);

        $messages = $filter->validation->validate($output);

        if ($messages === false) {
            throw new RuntimeException('参数验证错误');
        }

        if ($loose) {
            return [
                'default' => array_intersect_key($output, $oldFilters),
                'more' => array_diff_key($output, $oldFilters)
            ];
        }
        return $output;
    }

    /**
     * 默认查询参数处理
     * @param $data
     * @return array
     */
    public static function params($data):array
    {
        return self::handle([
            'size' => 'int',
            'page' => 'int',
            'order' => 'string',
            'fields' => 'string',
        ], $data, [
            'size' => '12',
            'page' => '1',
            'order' => 'id DESC',
            'fields' => ['*']
        ], true);
    }

    public static function make($value, $filter)
    {
        $data = ['value' => $value];
        $filter = ['value' => $filter];
        $output = self::handle($filter, $data);
        return isset($output['value']) ? $output['value'] : '';
    }

}
