<?php

namespace Itdashu\Easy;

use Itdashu\Easy\Forms\Element;

class Form
{
    public $type;
    public $name;
    public $options;
    public $userOptions;
    public $attributes;
    private $_elements;
    private $_filters;
    private $_validations;
    private $_value;
    private $_defalutValue;

    public function text(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function textarea(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function email(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function select(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function radio(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function checkbox(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function number(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function url(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function range(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function date(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function search(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function color(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        return $this->addElement(__METHOD__, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
    }

    public function getData($data){

    }

    private function addElement(string $name, $value = null, $default = null, array $options = array(), array $userOptions = array(), array $attributes = array(), array $filters = array(), array $validations = array())
    {
        $this->_elements[$name] = new Element($name, $value, $default, $options, $userOptions, $attributes, $filters, $validations);
        return $this->_elements[$name];
    }
}