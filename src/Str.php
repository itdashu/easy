<?php
/**
 * This file is part of HongWu soft.
 *
 * @link     https://www.hongwu.info
 * @document https://doc.hongwu.info
 * @contact  leno@itdashu.com
 * @license  https://www.hongwu.info/license.html
 */

namespace Itdashu\Easy;

class Str
{
    /**
     * 驼峰命名转下划线命名
     * @param string $str
     * @return string
     */
    public static function toUnderScore(string $str): string
    {
        $output = preg_replace_callback('/([A-Z]+)/', function ($match) {
            return '_' . strtolower($match[0]);
        }, $str);
        return trim(preg_replace('/_{2,}/', '_', $output), '_');
    }

    /**
     * 下划线命名转为驼峰命名
     * @param string $str
     * @return string
     */
    public static function toCamelCase(string $str): string
    {
        $array = explode('_', $str);
        $result = $array[0];
        $len = count($array);
        if ($len > 1) {
            for ($i = 1; $i < $len; $i++) {
                $result .= ucfirst($array[$i]);
            }
        }
        return $result;
    }

    /**
     * 自动闭合html标签
     *
     * @param string $html 需要闭合HTML标签的字符串
     * @return string
     */
    public static function closeHtmlTags(string $html): string
    {
        preg_match_all('#<(?!meta|img|br|hr|input\b)\b([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
        $openTags = $result[1];
        preg_match_all('#</([a-z]+)>#iU', $html, $result);
        $closeTags = $result[1];
        $openCount = count($openTags);
        $closeCount = count($closeTags);
        if ($closeCount == $openCount) {
            return $html;
        }
        $openTags = array_reverse($openTags);
        for ($i = 0; $i < $openCount; $i++) {
            if (!in_array($openTags[$i], $closeTags)) {
                $html .= '</' . $openTags[$i] . '>';
            } else {
                unset($closeTags[array_search($openTags[$i], $closeTags)]);
            }
        }
        return $html;
    }

    /**
     * 如果$str以$sub为结尾，返回true
     *
     * @param string $str 需要被检查的字符串
     * @param string $sub 搜索的字符串
     * @return bool
     */
    public static function endWith(string $str, string $sub): bool
    {
        return (substr($str, strlen($str) - strlen($sub)) == $sub);
    }

    /**
     * 如果$str以$sub为开头，返回true
     *
     * @param string $str
     * @param string $sub
     * @return bool bool
     */
    public static function startWith(string $str, string $sub): bool
    {
        return (substr($str, 0, strlen($sub)) == $sub);
    }

    /**
     * 判断是否是base64
     * @param string $str
     * @return bool
     */
    public static function isBase64(string $str)
    {
        return $str == base64_encode(base64_decode($str)) ? true : false;
    }

    /**
     * 生成指定长度的随机字符串
     * @param int $length 最终生成的随机字符串的长度
     * @param string $chars 随机字符池
     * @return string
     */
    public static function random(int $length, $chars = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM')
    {
        $hash = '';
        $max = strlen($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $hash .= $chars[mt_rand(0, $max)];
        }
        return $hash;
    }

    /**
     * 验证字符串是否是一个合法的域名
     * @param string $domain
     * @return bool
     */
    public static function isDomain(string $domain)
    {
        return !empty($domain) && strpos($domain, '--') === false &&
        preg_match('/^([a-z0-9]+([a-z0-9-]*(?:[a-z0-9]+))?\.)?[a-z0-9]+([a-z0-9-]*(?:[a-z0-9]+))?(\.us|\.tv|\.org\.cn|\.org|\.net\.cn|\.net|\.mobi|\.me|\.la|\.info|\.hk|\.gov\.cn|\.edu|\.com\.cn|\.com|\.co\.jp|\.co|\.cn|\.cc|\.biz)$/i', $domain) ? true : false;
    }

    /**
     * 自动转换字符串到指定编码，默认utf-8
     * @param string $data 需要被转换编码的字符串
     * @param string $encode 要转换的编码
     * @return string
     */
    public static function autoConvertEncoding(string $data, string $encode = 'utf-8')
    {
        if (!empty($data)) {
            $fileType = mb_detect_encoding($data, ['UTF-8', 'GBK', 'LATIN1', 'BIG5']);
            if ($fileType != 'UTF-8') {
                $data = mb_convert_encoding($data, $encode, $fileType);
            }
        }
        return $data;
    }

    /**
     * 根据前后文截取字符串
     * @param string $str
     * @param string $start
     * @param string $end
     * @return false|string
     */
    public static function cut(string $str, string $start, string $end)
    {
        $str = self::autoConvertEncoding($str);
        $output = mb_strstr($str, $start, false, 'utf8');
        $output = mb_strstr($output, $end, true, 'utf8');
        return $output;
    }

    /**
     * 截取字符串指定长度，并自动闭合字符串
     * @param string $string 需要被截取的字符串
     * @param int $length 截取的长度
     * @param bool $stripTags 是否剥除html标签并去除首尾的空格
     * @param int $start 是字符串哪里开始截取
     * @return string 被截取后的字符串
     */
    public static function sub(string $string, int $length, bool $stripTags = false, int $start = 0): string
    {
        if ($stripTags === true) {
            return strip_tags(trim(mb_substr($string, $start, $length, 'utf-8')));
        }
        $str = mb_substr($string, $start, $length, 'utf-8');
        return self::closeHtmlTags($str);
    }

    /**
     * 根据正则表达式截取内容
     * @param string $pattern 正则表达式
     * @param string $str 被截取的内容
     * @param int $offset 内容偏移量
     * @param bool $returnString 是否返回字符串结果
     * @param bool $autoConvertEncoding 自动转换编码至utf-8
     * @return array|string 被处理过的字符串
     */
    public static function pregStr(string $pattern, string $str, $offset = 0, bool $returnString = false, bool $autoConvertEncoding = true)
    {
        $output = [];
        if ($autoConvertEncoding) {
            $str = self::autoConvertEncoding($str);
        }

        preg_match($pattern, $str, $match);
        if (is_array($offset)) {
            foreach ($offset as $num) {
                if ($returnString) {
                    $matchString = implode(PHP_EOL, $match[$num]);
                    $output[$num] = $matchString;
                }
            }
        } else {
            $output = $match[$offset];
        }
        if (is_array($output) && $returnString) {
            $output = implode(PHP_EOL, $output);
        }
        return $output;
    }

    /**
     * 判断是否是手机号
     * @param $value
     * @return bool
     */
    public static function isPhone($value): bool
    {
        if (preg_match("/^1[345678]{1}\d{9}$/", $value)) {
            return true;
        } else {
            return false;
        }
    }
}
