<?php
/**
 * @return SimpleCWS
 */
function scws_new()
{
    /** @var SimpleCWS $tt */
    $tt = '';
    return $tt;
}

define('SCWS_XDICT_XDB', 1);
define('SCWS_MULTI_DUALITY', 1);
define('SCWS_XDICT_MEM', 1);
define('SCWS_XDICT_TXT', 1);
define('SCWS_MULTI_NONE', 1);
define('SCWS_MULTI_SHORT', 1);
define('SCWS_MULTI_DUALITY', 1);
define('SCWS_MULTI_ZMAIN', 1);
define('SCWS_MULTI_ZALL', 1);

interface SimpleCWS
{
    function close();

    function set_charset(string $charset): bool;

    function add_dict(string $dict_path, int $mode = SCWS_XDICT_XDB): bool;

    function set_dict(string $dict_path, int $mode = SCWS_XDICT_XDB): bool;

    function set_rule(string $rule_path): bool;

    function set_ignore(bool $yes): bool;

    function set_multi(int $mode): bool;

    function set_duality(bool $yes): bool;

    function send_text(string $text): bool;

    function get_result();

    function get_tops(int $limit = 10, string $xattr = '');

    function has_word(string $xattr): bool;

    function get_words(string $xattr);

    function version(): string;
}

;