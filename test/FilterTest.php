<?php
/**
 * Created by PhpStorm.
 * User: leno
 * Date: 18-5-5
 * Time: 下午12:27
 */

namespace ItdashuTest\Easy;


use Itdashu\Easy\Filter;
use PHPUnit\Framework\TestCase;

class FilterTest extends TestCase
{
    /**
     * @return Filter
     */
    public function testInt()
    {
        $filter = new Filter();
        $this->assertInstanceOf(Filter::class,$filter);
        return $filter;
    }

    /**
     * @param Filter $filter
     * @return Filter
     * @throws \Exception
     * @depends testInt
     */
    public function testAdd(Filter $filter){
        $filter->add('test',function (){
            return 'zz';
        });
        $filters = $filter->getFilters();
        $this->assertArrayHasKey('test',$filters,'add filter error');
        return $filter;
    }

    /**
     * @param Filter $filter
     * @throws \Exception
     * @depends testAdd
     */
    public function testSanitize(Filter $filter){
        $test = [
            'some(one)@exa\mple.com' => 'email',
            'hello<<' => 'string',
            '1e2342!' => 'int',
            '!100a019.01a' => 'float',
            '23fs@1sfd' => 'alphanum',
            'werw' => 'test'
        ];
        $testEnd = [
            'some(one)@exa\mple.com' => 'someone@example.com',
            'hello<<' => 'hello',
            '1e2342!' => '12342',
            '!100a019.01a' => '100019.01',
            '23fs@1sfd' => '23fs1sfd',
            'werw' => 'zz'
        ];
        foreach ($test as $name => $value){
            $output = $filter->sanitize($name,$value);
            $isTure = $output == $testEnd[$name];
            $isTure = (bool)$isTure;
            $this->assertTrue($isTure,"{$value} filter false");
        }
    }
}