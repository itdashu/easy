<?php
/**
 * Created by PhpStorm.
 * User: leno
 * Date: 18-5-5
 * Time: 下午4:29
 */

namespace ItdashuTest\Easy;


use Itdashu\Easy\Validation;
use PHPUnit\Framework\TestCase;

class ValidationTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testInit(){
        $validation = new Validation();
        $this->assertInstanceOf(Validation::class,$validation,'非法的'.Validation::class.'实例');
    }

    /**
     * @param Validation $validation
     * @return Validation
     * @throws \Exception
     * @depends testInit
     */
    public function testAdd(Validation $validation){
        $validation->add('email',new Validation\Validator\Email(
            [
                'message' => '不是一个合法的邮件'
            ]
        ));
        $validation->add('ip',new Validation\Validator\Ip());
        $validation->add('alnum',new Validation\Validator\Alnum());
        $validation->add('alpha',new Validation\Validator\Alpha());
        $validation->add('between',new Validation\Validator\Between());
        $validation->add('callback',new Validation\Validator\Callback());
        $validation->add('conf',new Validation\Validator\Confirmation());
        $validation->add('card',new Validation\Validator\CreditCard());
        $validation->add('date',new Validation\Validator\Date());
        $validation->add('digit',new Validation\Validator\Digit());
        $validation->add('exclu',new Validation\Validator\ExclusionIn());
        $validation->add('file',new Validation\Validator\File());
        $validation->add('iden',new Validation\Validator\Identical());
        $validation->add('inclu',new Validation\Validator\InclusionIn());
        $validation->add('num',new Validation\Validator\Numericality());
        $validation->add('presence',new Validation\Validator\PresenceOf());
        $validation->add('regex',new Validation\Validator\Regex());
        $validation->add('strleng',new Validation\Validator\StringLength());
        $validation->add('url',new Validation\Validator\Url());
        return $validation;
    }

    /**
     * @param Validation $validation
     * @throws \Exception
     * @depends testAdd
     */
    public function testValidatorYes(Validation $validation){
        $data = [
            'ip' => '127.0.0.1',
            'alnum' => '12d3df',
            'alpha' => '23sd',
            'between' => 'test',
            'callback' => '111',
            'conf' => '111',
            'card' => '9558800200135073266',
            'date' => '2018-01-01',
            'digit' => '1000',
            'exclu' => 'test',
            'file' => 'aa.php',
            'iden' => 'yes',
            'inclu' => 'ssss',
            'num' => 12312,
            'presence' => 'a',
            'regex' => 'abc123',
            'strleng' => '1231dd',
            'url' => 'http://www.itdashu.com/aa.html'
        ];
        $validation->validate($data);
        $messages = $validation->getMessages();
        $messagesCount = $messages->count();
        $messagesOutput = '';
        foreach ($messages as $message){
            $messagesOutput .= $message->getMessage().PHP_EOL;
        }
        $this->assertEquals(0,$validation->getMessages()->count(),$messagesOutput);
    }

    /**
     * @param Validation $validation
     * @throws \Exception
     * @depends testAdd
     */
    public function testValidatorNo(Validation $validation){
        $data = [
            'ip' => '127.0.0.a1',
            'alnum' => '12d3df!',
            'alpha' => '23s*d',
            'between' => 'te*st',
            'callback' => '11a1',
            'conf' => '111&',
            'card' => '9558108800200135073266',
            'date' => '2018-01o-01',
            'digit' => '10000',
            'exclu' => 'tes0t',
            'file' => 'aa.ph0p',
            'iden' => 'ye8s',
            'inclu' => 'ss8ss',
            'num' => '1212312d',
            'presence' => 'aad',
            'regex' => 'abc121d3',
            'strleng' => '1231sddd',
            'url' => '://www.itdashu.com/aa.html'
        ];
        $validation->validate($data);
        $this->assertEquals(count($data),$validation->getMessages()->count(),'validator false data not pass');
    }

    /**
     * @param Validation $validation
     * @throws \Exception
     * @depends testAdd
     */
    public function testFilter(Validation $validation){
        $data = [
            'ip' => '127.0.0.a1',
            'alnum' => '12d3df!',
            'alpha' => '23s*d',
            'between' => 'te*st',
            'callback' => '11a1',
            'conf' => '111&',
            'card' => '9558108800200135073266',
            'date' => '2018-01o-01',
            'digit' => '10000',
            'exclu' => 'tes0t',
            'file' => 'aa.ph0p',
            'iden' => 'ye8s',
            'inclu' => 'ss8ss',
            'num' => '1212312d',
            'presence' => 'aad',
            'regex' => 'abc121d3',
            'strleng' => '1231sddd',
            'url' => '://www.itdashu.com/aa.html'
        ];
        $validation->validate($data);
        $this->assertEquals(count($data),$validation->getMessages()->count(),'validator false data not pass');
    }
}